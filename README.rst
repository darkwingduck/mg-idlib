Summary
=======
Generates character friendly and regex based internal and public identifiers.

Roadmap
=======
* 0.1.0: Current

 - Create unit test friendly id generation
 
 - Create unit test for regex id generation
 
 - Create unit test for parsing id based on regex
 
 - Release to pypi instance.

* 0.0.3:

 - Create method to generate id based on regular expression
 
 - Create method to parse id using regular expression

* 0.0.2:

 - Create method to generate character friendly id
 
* 0.0.1:

 - Create setup.py for use with easy_install and pip
 