#!/usr/bin/python


from setuptools import setup, find_packages
import idlib as PACKAGE


setup(
    name = PACKAGE.PACKAGE_NAME,
    version = PACKAGE.VERSION,
    packages = find_packages(),
    include_package_data = True,
    install_requires = PACKAGE.REQUIRED_PACKAGES,
    author = PACKAGE.CONTRIBUTERS[0][0],
    author_email = PACKAGE.CONTRIBUTERS[0][1],
    description = PACKAGE.SHORT_DESCRIPTION,
    long_description = PACKAGE.LONG_DESCRIPTION,
    keywords = PACKAGE.KEYWORDS,
    zip_safe=False
)