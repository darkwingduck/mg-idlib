"""
Defines all of the TestCases that should be tested for verification.
"""


from __future__ import unicode_literals
import unittest
from idlib.tests.friendly import FriendlyTestCase
from idlib.tests.regex import RegexTestCase


if __name__ == "__main__":
    unittest.main()