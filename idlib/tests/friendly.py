"""
Defines unit test for friendly id generation.
"""


from __future__ import unicode_literals
import unittest
from idlib.friendly import (generate, DEFAULT_CHARACTERS, EXTENDED_CHARACTERS,
                            EXTRA_CHARACTERS)


class FriendlyTestCase(unittest.TestCase):
    def test_generate(self):
        """
        @summary: Test that the generate method returns a non empty unicode
        string.
        """
        
        default_id = generate(length=10,
                              characters=DEFAULT_CHARACTERS,
                              validate_callback=None)
        
        self.assertIsInstance(default_id, unicode)
        self.assertTrue(default_id)
    
    
    def test_generate_len(self):
        """
        @summary: Test that the number of characters in the id matches the
        requested length.
        """
        
        default_id = generate(length=10,
                              characters=DEFAULT_CHARACTERS,
                              validate_callback=None)

        self.assertEqual(len(default_id), 10)
    
    
    def test_generate_all_characters_in_default_character_set(self):
        """
        @summary: Test that all of the characters in the generated if are in the
        default character set.
        """
        
        default_id = generate(length=10,
                              characters=DEFAULT_CHARACTERS,
                              validate_callback=None)
        
        for c in default_id:
            self.assertIn(c, DEFAULT_CHARACTERS)
    
    
    def test_generate_all_characters_in_extended_character_set(self):
        """
        @summary: Test that all of the characters in the generated if are in the
        extended character set.
        """
        
        extended_id = generate(length=10,
                              characters=EXTENDED_CHARACTERS,
                              validate_callback=None)
        
        for c in extended_id:
            self.assertIn(c, EXTENDED_CHARACTERS)
    
    
    def test_validation_callback(self):
        """
        @summary: Test that the validation callback is working and that the
        returned id is not in the list of characters used by the validation
        callback.
        """
        
        ALREADY_IN_USE = ["aaUASdd783", "Qe6BYaP09"]
        
        def validate(dummy):
            return not (dummy in ALREADY_IN_USE)
        
        default_id = generate(length=10,
                              characters=DEFAULT_CHARACTERS,
                              validate_callback=validate)
        
        self.assertNotIn(default_id, ALREADY_IN_USE)
    
    
    def test_must_include_from(self):
        """
        @summary: Test that the id returned includes at least three of the
        extended characters.  This test may take a while.
        """
        
        #checking the default character set
        extended_id = generate(length=10,
                              characters=EXTENDED_CHARACTERS,
                              validate_callback=None,
                              must_include_from=EXTRA_CHARACTERS,
                              min_include_from=3)
        
        common = set(extended_id) & set(EXTRA_CHARACTERS)

        self.assertGreaterEqual(len(common), 3)