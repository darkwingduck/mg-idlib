"""
Defines unit test for regex id generation and parsing
"""


from __future__ import unicode_literals
import unittest
from idlib.regex import generate, parse
from exceptions import KeyError, IndexError, TypeError


class RegexTestCase(unittest.TestCase):
    def test_generate(self):
        """
        @summary: Test that the generate method returns a non empty unicode
        string with the expected value.
        """
        
        default_id = generate(pattern=r"(?P<site>\w+)-(?P<public>\w+)",
                              site="ABCDEF",
                              public="abcde")
        
        self.assertIsInstance(default_id, unicode)
        self.assertTrue(default_id)
        self.assertEqual("ABCDEF-abcde", default_id)
    
    
    def test_generate_type_error(self):
        """
        @summary: Test that a type error is raised when the incorrect type is
        given.
        """
        
        with self.assertRaises(TypeError):
            generate(pattern=r"(?P<site>[a-zA-z]+)-(?P<public>\w+)",
                     site=12345,
                     public="abcde")
    
    
    def test_generate_key_error(self):
        """
        @summary: Test that a key error is raised when the key is missing.
        """
        
        with self.assertRaises(KeyError):
            generate(pattern=r"(?P<site>[a-zA-z]+)-(?P<public>\w+)",
                     public="abcde")
    
    
    def test_generate_index_error(self):
        """
        @summary: Test that a index error is raised when the positional argument
        is missing.
        """
        
        with self.assertRaises(IndexError):
            generate(pattern=r"([a-zA-z]+)-(?P<public>\w+)", 
                     public="abcde")
    
    
    def test_parse_kwargs(self):
        """
        @summary: Test that the parse method returns a tuple containing the
        positional and named arguments from the pattern.
        """
        
        args, kwargs = parse(pattern=r"(?P<site>\w+)-(?P<public>\w+)",
                              value="ABCDEF-abcde")
        
        self.assertIsInstance(args, list)
        self.assertIsInstance(kwargs, dict)
        self.assertFalse(args)
        self.assertIn("site", kwargs)
        self.assertIn("public", kwargs)
        self.assertEqual(kwargs["site"], "ABCDEF")
        self.assertEqual(kwargs["public"], "abcde")
    
    
    def test_parse_args(self):
        """
        @summary: Test that the parse method returns a tuple containing the
        positional and named arguments from the pattern.
        """
        
        args, kwargs = parse(pattern=r"(\w+)-(\w+)",
                              value="ABCDEF-abcde")
        
        self.assertIsInstance(args, list)
        self.assertIsInstance(kwargs, dict)
        self.assertTrue(args)
        
        for arg in args:
            self.assertIsInstance(arg, unicode)
    
    
    def test_parse_args_kwargs(self):
        """
        @summary: Test that the parse method returns a tuple containing the
        positional and named arguments from the pattern.
        """
        
        args, kwargs = parse(pattern=r"(\w+)-(?P<public>\w+)",
                              value="ABCDEF-abcde")
        
        self.assertIsInstance(args, list)
        self.assertIsInstance(kwargs, dict)
        self.assertTrue(args)
        
        for arg in args:
            self.assertIsInstance(arg, unicode)
        
        self.assertIn("public", kwargs)
        self.assertEqual(kwargs["public"], "abcde")
    
    
    def test_parse_value_error(self):
        """
        @summary: Test that the parse method throws an exception when one of the
        groups is missing.
        """
        
        with self.assertRaises(ValueError):
            parse(pattern=r"(\w+)-(?P<public>\w+)", value="-abcde")
        
        with self.assertRaises(ValueError):
            parse(pattern=r"(\w+)-(?P<public>\w+)", value="ABCDEF-")