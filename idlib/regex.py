"""
Defines methods for generating and parsing id's based on regular expressions.
"""


from __future__ import unicode_literals
from exceptions import KeyError, IndexError, TypeError
import re


def generate(pattern, *args, **kwargs):
    """
    @summary: Generates a id based on the given regular expression pattern.
    
    This will also enforce the expressions in the pattern groupings.
    
    @type pattern: String
    @param route: Regular expression with groups.

    @type args: List
    @param args: List of positional arguments which will be inserted into
    the id.
    
    @type kwargs: Dict
    @param kwargs: Dict containing keyword arguments which will be inserted
    into the id.
    
    @rtype: Unicode
    @return: Constructed id with the regular expressions groups replaced by
    the given args and kwargs.
    
    @raise KeyError: Raised if a named group in the pattern does not have a
    value defined in **kwargs.
    
    @raise IndexError: Raised if a non-named argument is still required after
    all arguments from *args have been used.
    
    @raise TypeError: Raised if an argument does not match the regular
    regular expression it is replacing.
    """
    
    #we want args to be a list so we can treat it as a stack
    args = map(None, args)
    
    def part(match):
        contents = match.group(1)
        name = re.search(r"<(.*?)>", contents)

        if name:
            key = name.group(1)
            exp = contents[len(key) + 4:]
            
            if key in kwargs:
                value = unicode(kwargs[key])
                
                if re.match(exp, value):
                    return value
                
                else:
                    raise TypeError("'%s' expected to match '%s'" % (key, exp))
            
            else:
                raise KeyError("'%s' missing" % key)
        
        else:
            exp = contents
            
            if args:
                value = unicode(args.pop(0))
                
                if re.match(exp, value):
                    return value
                
                else:
                    raise TypeError("'%s' expected to match '%s'" % (key, exp))
                
            else:
                raise IndexError("Not enough positional arguments")
    
    return re.sub(r"\((.*?)\)", part, pattern)


def parse(pattern, value):
    """
    @summary: Parses the given value based on the pattern.
    
    @type pattern: String
    @param route: Regular expression with groups.
    
    @type value: Unicode
    @param value: ID being parsed by the pattern.
    
    @rtype: Tuple
    @return: Tuple containing a list of positional arguments parsed and dict
    containing the named patterns parsed.  Please note that all values in the
    positional and dict are unicode.
    
    @raise KeyError: Raised if the number of named groups found does not match
    the number of named groups in the pattern.
    
    @raise IndexError: Raised if the number of positional arguments found does
    not match the number of positional arguments in the pattern.
    
    @raise TypeError: Raised if the value cannot be matched against the pattern.
    """
    
    reg = re.compile(pattern)
    named_groups = reg.groupindex
    num_positional_groups = reg.groups - len(named_groups)
                            
    match = reg.match(value)
    
    if match:
        named_found = match.groupdict()
        positional_found = []
        
        named_group_positions = named_groups.values()
        
        for i, value in enumerate(match.groups()):
            if (i+1) not in named_group_positions:
                positional_found.append(unicode(value))
        
        num_positional_found = len(positional_found)
        
        #making sure the same number of keys are found in the pattern and value
        if (len(named_groups) - len(named_found)) != 0:
            raise KeyError("Keys from value do not match keys from pattern")
        
        #making sure the postitional arguments found are the same number in the
        #pattern
        if (num_positional_groups - num_positional_found) != 0:
            raise IndexError("Value does not have the same number of "
                             "positional arguments as the pattern")
        
        for key, value in named_found.items():
            named_found[key] = unicode(value)
        
        return positional_found, named_found
    
    else:
        raise ValueError("Value given does not match the pattern")