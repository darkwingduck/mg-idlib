"""
Defines methods for creating user friendly identifiers.
"""


from __future__ import unicode_literals
import string, random


"""
Defines the default character set to be used when generating public ids
"""
DEFAULT_CHARACTERS = "%s%s" % (string.ascii_letters, string.digits)


"""
Defines the extra characters used when creating the extended character set
"""
EXTRA_CHARACTERS = "`~!@#$%^&*()_-+=<>,.?/:;[]{}|\\'\""


"""
Defines an extended set of characters to be used when generating public ids.
"""
EXTENDED_CHARACTERS = "%s%s" % (DEFAULT_CHARACTERS, EXTRA_CHARACTERS)


def generate(length, characters=DEFAULT_CHARACTERS, validate_callback=None,
             must_include_from=None, min_include_from=1):
    """
    @summary: Generates a friendly id of a given length.
    
    @type length: Int
    @param length: How many characters the id should be.
    
    @type characters: Unicode
    @param characters: Unicode string containing the characters to create the id
    with.
    
    @type validate_callback: Function
    @param validate_callback: Function that can be used to make sure the id is
    unique or passes some other form of validation.
    
    The function should take a unicode string as the parameter and return True
    if the id is valid or False if the id is invalid.  Example:
            
        def validate_id(id):
            return not (id in ["something"])
    
    @type must_include_from: Unicode
    @param must_include_from: Unicode string containing a sequence of characters
    where one or more character in the sequence must be present in the generated
    id.
    
    @type min_include_from: Int
    @param min_include_from: Minimum number of individual characters from
    must_include_from that must be included.
    
    @rtype: Unicode
    @return: Generated id that passes the validation_callback.
    """
    
    generated = ""
    selected = []
    
    while True:
        del selected[:]
        
        while len(selected) < length:
            selected.append(random.choice(characters))
        
        generated = "".join(selected)
        
        if must_include_from:
            common = set(generated) & set(must_include_from)

            if len(common) < min_include_from:
                continue
        
        if validate_callback:
            if validate_callback(generated):
                break
        
        else:
            break
    
    return generated
